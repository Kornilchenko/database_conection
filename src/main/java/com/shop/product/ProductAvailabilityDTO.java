package com.shop.product;

import jakarta.validation.constraints.NotNull;

public class ProductAvailabilityDTO {
    @NotNull
    private int id_product;
    @NotNull
    private int id_shop;
    @NotNull
    private int quantity;

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId_shop() {
        return id_shop;
    }

    public void setId_shop(int id_shop) {
        this.id_shop = id_shop;
    }

    public String getNameTable() {
        return "product_availability";
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    /*@Override
    public String toString(){
        return id_product +","+ id_shop +","+ quantity;
    }*/
}
