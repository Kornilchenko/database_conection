package com.shop.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReaderArguments {
    private static final Logger log = LoggerFactory.getLogger(ReaderArguments.class);
    public int getTotalNumberOfLines() {
        return totalNumberOfLines;
    }

    public String getValueForSelect() {
        return valueForSelect;
    }

    private int totalNumberOfLines =-1;
    private String valueForSelect = "";
    public ReaderArguments(String[] args) {
        setProperties(args);
    }
    private void setProperties(String[] args){
        log.info("reading string arguments");
        for (String arg : args) {
            if (arg.contains("count")) {
                totalNumberOfLines = Integer.parseInt(arg.substring(arg.indexOf("=")+1));
            }
            if (arg.contains("type")) {
                valueForSelect = arg.substring(arg.indexOf("=")+1);
            }
        }
        if(totalNumberOfLines == -1) totalNumberOfLines = 100;
        if(valueForSelect.equals("")) valueForSelect = "type 2";
    }
}
