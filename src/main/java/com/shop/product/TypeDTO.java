package com.shop.product;

import jakarta.validation.constraints.NotNull;

public class TypeDTO {
    @NotNull
    private int id;
    @NotNull
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNameTable() {
        return "type_product";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

 /*   @Override
    public String toString() {
        return id + "," + type;
    }*/
}
