package com.shop.product;

import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Range;

public class ProductDTO {
    @NotNull
    private int id;
    @NotNull
    //@CheckCase(value = CaseMode.UPPER)
    private String name;
    @NotNull
    private float cost;
    @NotNull
    @Range(min=1, max = 55, message = "size mast be from 1 to 55")
    private int type;

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameTable() {
        return "product";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

/*    @Override
    public String toString(){
        return id+","+name+","+ cost+","+type;
    }*/
}
