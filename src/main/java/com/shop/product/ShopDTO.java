package com.shop.product;

import jakarta.validation.constraints.NotNull;

public class ShopDTO {
    @NotNull
    private int id;
    @NotNull
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNameTable() {
        return "shop";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   /* @Override
    public String toString(){
        return id+","+ address;
    }*/
}

