package com.shop.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ReaderProperties {
    private static final Logger log = LoggerFactory.getLogger(ReaderProperties.class);
    private final HashMap<String, String> property;

    /**
     *
     * @param externalPropertiesFile -external file to read properties
     * @throws IOException - error reading external file
     */
    public ReaderProperties(String externalPropertiesFile) throws IOException {
        this(externalPropertiesFile, "aplication.properties");
    }

    /**
     * class constructor
     *
     * @param externalPropertiesFile - external file to read properties
     * @param internalPropertiesFile - internal file to read properties
     * @throws IOException - error reading external file
     */
    public ReaderProperties(String externalPropertiesFile, String internalPropertiesFile) throws IOException {
        //log.info("run constructor ReaderProperties class");
        property = new HashMap<>();
        property.put("datasource.url", null);
        property.put("datasource.username", null);
        property.put("datasource.password", null);
        property.put("database.name", null);
        Properties properties = readExternalFile(externalPropertiesFile);
        boolean reads = readPropertiesFromMyFile(properties);
        if (!reads) {
            properties = readInternalFile(internalPropertiesFile);
            reads = readPropertiesFromMyFile(properties);
        }
        if(!reads){
            for (Map.Entry<String, String> item : property.entrySet()) {
                item.setValue(null);
            }
            log.debug("no required properties");
        }
    }

    /**
     * reading an internal properties file
     *
     * @return - properties file
     * @throws IOException - error reading external file
     */
    private Properties readInternalFile(String filename) throws IOException {
        Properties properties = new Properties();
        log.debug("read internal file properties");
        InputStream inputStream = ReaderProperties.class.getClassLoader().getResourceAsStream(filename);
        assert inputStream != null;
        properties.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        return properties;
    }

    /**
     * reading an external properties file
     *
     * @param fileName - external file to read properties
     * @return - properties file
     */
    private Properties readExternalFile(String fileName) {
        Properties properties = new Properties();
        log.info("read external file properties");
        try (
                FileInputStream fis = new FileInputStream(fileName);
                InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8)
        ) {
            properties.load(isr);
        } catch (IOException e) {
            log.error("Error, read external file! ");
        }
        return properties;
    }

    /**
     * reads properties from a file
     *
     * @param properties - properties written in the file
     * @return - true if the required property keys exist and they contain numeric values
     */
    private boolean readPropertiesFromMyFile(Properties properties) {
        //log.info("check the properties file for the required values");
        for (Map.Entry<String, String> item : property.entrySet()) {
            String temp = properties.getProperty(item.getKey());
            if (temp == null) {
                log.debug("properties file does not contain required properties");
                return false;
            }
            item.setValue(temp);
        }
        log.info("properties from the file are read");
        return true;
    }

    public String getURL(){
        return property.get("datasource.url");
    }
    public String getUser(){
        return property.get("datasource.username");
    }
    public String getPassword(){
        return property.get("datasource.password");
    }
    public String getNameDatabase(){
        return property.get("database.name");
    }
    public String getStringConnect(){
        return getURL() + "/" + getNameDatabase()+", "+ getUser()+", "+ getPassword();
    }
}
