package com.shop.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
Створити таблиці DDL-скриптом, в яких будуть всі товари всіх магазинів Епіцентр
За допомогою JDBC нагенерити рандомно як мінімум 10 мільйонів товарів
Перед вставкою в БД перевіряти DTO на валідність використовуючи hibernate-validator
Видавати адресу магазину із найбільшою кількістю товарів якогось типу, а тип товару
запитувати із консолі, параметром командного рядка (для автозапуску Дженкінсом після
білда можна параметризувати джобу параметром ‘Тип_товару’)
Доп. задача: розпаралелити заради прискорення роботи генерацію і валідацію,
використовуючи пакет java.concurrency
 */
public class ShopDatabase {

    private static final Logger log = LoggerFactory.getLogger(ShopDatabase.class);
    public static void main(String[] args) throws IOException, SQLException {
        long start = System.currentTimeMillis(); //start time

        DataGenerator dg = new DataGenerator();
        TypeDTO type = new TypeDTO();
        ProductDTO product = new ProductDTO();
        ShopDTO shop =new ShopDTO();
        ProductAvailabilityDTO productAvailability = new ProductAvailabilityDTO();

        List<String> table = createTableQuery();
        ConnectionDatabase connectionDB = new ConnectionDatabase("database.properties");
        connectionDB.create(table);
        ReaderArguments arguments = new ReaderArguments(args);
        int total = arguments.getTotalNumberOfLines();
        int linesType = total/10000;
        if(linesType < 5) linesType = 5;
        int linesProduct = total/1000;
        if(linesProduct < 10) linesProduct = 10;
        int linesShop = total/10000;
        if(linesShop < 10) linesShop = 10;
        int lineProductAvailability = total - linesProduct - linesShop - linesType;

        ConnectionDatabase.maximumLines = 10000;
        connectionDB.insert(dg.generateForTypeProduct(linesType, type));
        connectionDB.insert(dg.generateForProduct(linesProduct, product, linesType));
        connectionDB.insert(dg.generateForShop(linesShop, shop));
        connectionDB.insert(dg.generateForProductAvailability(lineProductAvailability,
                productAvailability, linesShop, linesProduct));
        connectionDB.select(arguments.getValueForSelect());
        connectionDB.closeConnection();

        long stop = System.currentTimeMillis();
        long res = stop - start;
        log.info("time programm: {}min: {}sec: {}msec", res/60000, res%60000/1000, res%1000);
    }


    public static List<String> createTableQuery() {
        List<String> createTable = new ArrayList<>();
      /*      String script = Files.readString(Path.of("src/main/resources/create_table.sql"));
            log.info(script);
*/
        createTable.add("drop table if exists product_availability");
        createTable.add("drop table if exists shop");
        createTable.add("drop table if exists product");
        createTable.add("drop table if exists type_product");
        createTable.add("CREATE TABLE IF NOT EXISTS type_product(" +
                "id INT NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY," +
                " type VARCHAR(50) UNIQUE NOT NULL)");
        createTable.add("CREATE TABLE IF NOT EXISTS product(" +
                "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                "name VARCHAR(50) UNIQUE NOT NULL, " +
                "cost DECIMAL(6, 2) NOT NULL," +
                "type INT NOT NULL," +
                "FOREIGN KEY (type)" +
                "REFERENCES type_product (id))");
        createTable.add("CREATE TABLE IF NOT EXISTS shop (" +
                "  id INT NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY," +
                "  address VARCHAR(50) UNIQUE NOT NULL)");
        createTable.add("CREATE TABLE IF NOT EXISTS product_availability (" +
                "  id_product INT NOT NULL," +
                "  id_shop INT NOT NULL," +
                "  quantity INT NOT NULL," +
                "    FOREIGN KEY (id_product)" +
                "    REFERENCES product (id)" +
                "    ON DELETE CASCADE" +
                "    ON UPDATE CASCADE," +
                "    FOREIGN KEY (id_shop)" +
                "    REFERENCES shop (id)" +
                "    ON DELETE CASCADE" +
                "    ON UPDATE CASCADE)");
        return createTable;
    }
}
