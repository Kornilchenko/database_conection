package com.shop.product;

import java.util.ArrayList;
import java.util.List;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class DataGenerator {
    private static final Logger log = LoggerFactory.getLogger(DataGenerator.class);

    public List<String> generateForTypeProduct(int quantity, TypeDTO type) {
        log.info("Create an array of strings to be inserted into the product type table. {} lines", quantity);
        List<String> listForInsert = new ArrayList<>(quantity);
        listForInsert.add(type.getNameTable());
        int count = 0;
        int countLines=1;
        while (quantity >= countLines) {
            ++count;
            if(count%10000 == 0) log.info("creation and validation of {} elements ",count);
            type.setId(Math.toIntExact(countLines));
            type.setType("type " + count);
            if(validator(type)) {
                ++countLines;
                listForInsert.add("'"+type.getId()+"','"+type.getType()+"'");
            }
        }
        log.info("end of array creation to create table product type");
        return listForInsert;
    }

    public List<String> generateForProduct(int quantity, ProductDTO product, int foreginKeyType) {
        log.info("Create an array of strings to be inserted into the product table. {} lines", quantity);
        List<String> listForInsert = new ArrayList<>(quantity);
        listForInsert.add(product.getNameTable());
        Random random = new Random();
        int countLines = 1;
        while (quantity >= countLines) {
            if(countLines%10000 == 0) log.info("creation and validation of {} elements ",countLines);
            product.setId(countLines);
            product.setName("product " + RandomStringUtils.randomAlphanumeric(10));
            product.setType(random.nextInt(foreginKeyType)+1);
            product.setCost(random.nextInt(270) + random.nextFloat());
            if(validator(product)) {
                ++countLines;
                listForInsert.add("'"+product.getId()+"','"+product.getName()+
                        "','"+product.getCost()+"','"+product.getType()+"'");
            }
        }
        log.info("end of array creation to create table product");
        return listForInsert;
    }

    public List<String> generateForShop(int quantity, ShopDTO shop) {
        log.info("Create an array of strings to be inserted into the shop table. {} lines", quantity);
        List<String> listForInsert = new ArrayList<>(quantity);
        listForInsert.add(shop.getNameTable());
        int count = 0;
        int countLines = 1;
        while (quantity >= countLines) {
            ++count;
            if(count%10000 == 0) log.info("creation and validation of {} elements ",count);
            shop.setId(countLines);
            shop.setAddress("address " + count);
            if(validator(shop)) {
                ++countLines;
                listForInsert.add("'"+shop.getId()+"','"+shop.getAddress()+"'");
            }
        }
        log.info("end of array creation to create table shop");
        return listForInsert;
    }

    public List<String> generateForProductAvailability(int quantity, ProductAvailabilityDTO productAvailability,
                                                       int keyShop, int keyProduct) {
        log.info("Create an array of strings to be inserted into the product availability table. {} lines", quantity);
        List<String> listForInsert = new ArrayList<>(quantity);
        listForInsert.add(productAvailability.getNameTable());
        Random random = new Random();
        int countLines = 1;
        while (quantity >= countLines) {
            if(countLines%10000 == 0) log.info("creation and validation of {} elements ",countLines);
            productAvailability.setId_product(random.nextInt(keyProduct) +1);
            productAvailability.setId_shop(random.nextInt(keyShop)+1);
            productAvailability.setQuantity(random.nextInt(175));
            if(validator(productAvailability)) {
                ++countLines;
                listForInsert.add("'"+productAvailability.getId_product()+"','"+productAvailability.getId_shop()+
                        "','"+productAvailability.getQuantity()+"'");
            }
        }

        log.info("end of array creation to create table product availability");
        return listForInsert;
    }

    public boolean validator(Object objectDTO) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator.validate(objectDTO).isEmpty();
    }
}