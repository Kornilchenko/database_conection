/*
package com.shop.product;

import org.hsqldb.server.Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ServerConnect {

 */
/*В документации HSQLDB есть инструкция командной строки для запуска сервера HSQLDB
      ( HSQLDB Doc ). Но есть это свойство «file:mydb», поэтому я предполагаю,
      что оно не в режиме только для памяти.*//*


    Server server = new Server();
server.setDatabaseName(0,"mainDb");
server.setDatabasePath(0,"mem:mainDb");
server.setDatabaseName(1,"standbyDb");
server.setDatabasePath(1,"mem:standbyDb");
server.setPort(9001); // this is the default port
server.start();

    String url="jdbc:hsqldb:hsql://172.0.0.1:9001/mainDb";
Class.forName("org.hsqldb.jdbc.JDBCDriver");
    Connection conn = DriverManager.getConnection(url, "SA", "");

    public ServerConnect() throws SQLException {
    }

*/
/*где 192.168.5.1 — это IP-адрес сервера, на котором работает HSQL. Чтобы подключиться
    к standbyDb, замените mainDb на standbyDb в первой строке. Как только вы получите соединение,
    вы можете выполнять все операции, связанные с базой данных.
Вот что вам нужно сделать, чтобы подключиться к серверу удаленно с помощью DatabaseManagerSwing.
Загрузите банку hsqldb-xxx и скопируйте ее в папку (xxx — это версия), откройте
терминал или командную строку, перейдите в папку и запустите
java -cp hsqldb-x.x.x.jar org.hsqldb.util.DatabaseManagerSwing
Выберите «Сервер ядра базы данных HSQL» в раскрывающемся списке «Тип» и
 укажите «jdbc:hsqldb:hsql://192.168.5.1:9001/mainDb» в качестве URL-адреса.
 Это соединит вас с удаленным экземпляром сервера HSQL в памяти.*//*


}
*/
