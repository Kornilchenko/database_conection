package com.shop.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.util.List;

public class ConnectionDatabase {
    ReaderProperties properties;
    private final Logger log = LoggerFactory.getLogger(ConnectionDatabase.class);
    private final String url;
    private final String username;
    private final String password;
    private final String dataBaseName;
    private Statement statement;
    private Connection connection;
    public static int maximumLines = 7000;

    public ConnectionDatabase(String externalProperties) throws IOException {
        properties = new ReaderProperties(externalProperties);
        url = properties.getURL();
        username = properties.getUser();
        password = properties.getPassword();
        dataBaseName = properties.getNameDatabase();
        databaseConnection();
    }

    private void databaseConnection() {
        log.info("database connection");
        try {
            connection = DriverManager.getConnection(url + "/" + dataBaseName, username, password);
            statement = connection.createStatement();
        } catch (SQLException ex) {
            log.error("connection failed! "+ ex.getMessage());
        }
        log.info("connection successful");
    }

    public void insert(List<String> requests) throws SQLException {
        String name_table = requests.get(0);
        log.info("inserting elements into a table \"{}\"", name_table);
        int count = 0;
        int n = 1;
        while (n < requests.size()) {
            statement.addBatch("INSERT INTO " + name_table + " VALUES(" + requests.get(n++) + ")");
            if (++count%maximumLines == 0) {
                log.info("inserting {} element into a table \"{}\"",n, name_table);
                statement.executeBatch();
                count = 0;
            }
        }
        statement.executeBatch();
    }

    public void create(List<String> requests) throws SQLException {
        log.info("creating tables");
        for (String request : requests) {
            statement.execute(request);
        }
    }

    public void select(String type) throws SQLException {
        log.info("executing a select query");
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT S.address, SUM(quantity) as quantity" +
                " FROM shop S join product_availability PA on S.id = PA.id_shop" +
                " join product P on PA.id_product = P.id" +
                " join type_product T on P.type = T.id" +
                " where T.type = ?" +
                " group by id_shop " +
                " LIMIT 1");
        preparedStatement.setString(1, type);
        ResultSet result = preparedStatement.executeQuery();
        while (result.next()) {
            log.info("the address of the store in which there are more " +
                    "other products of the type '" + type + "' = " + result.getString("address"));
        }
        result.close();
    }

    public void closeConnection() throws SQLException {
        statement.close();
        connection.close();
    }
}
/*
          ResultSet rs = statement.executeQuery("SELECT * FROM user WHERE surname = 'groza'");
          while (rs.next()) {
              log.info("id " + rs.getInt("id") + ";" +
                      " name " + rs.getString("name") + ";" +
                      " Surname " + rs.getString("surname") + ";");
          }
          rs.close(); //закрытие тотока
    /*ResultSet resultSets = statement.executeQuery("SELECT * FROM user");
          while (resultSets.next()) {
                  log.info("id " + resultSets.getInt("id") + ";" +
                  " name " + resultSets.getString("name") + ";" +
                  " Surname " + resultSets.getString("surname") + ";");
                  }
*/