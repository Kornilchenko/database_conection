drop table if exists product_availability;
drop table if exists shop;
drop table if exists product;
drop table if exists type_product;
CREATE TABLE IF NOT EXISTS type_product(id INT NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
                 type VARCHAR(50) UNIQUE NOT NULL);

CREATE TABLE IF NOT EXISTS product(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(50) UNIQUE NOT NULL,
                cost DECIMAL(6, 2) NOT NULL,
                type INT NOT NULL,
                FOREIGN KEY (type)
                REFERENCES type_product (id));

CREATE TABLE IF NOT EXISTS shop (id INT NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
                address VARCHAR(50) UNIQUE NOT NULL);

CREATE TABLE IF NOT EXISTS product_availability (id_product INT NOT NULL,
                 id_shop INT NOT NULL,
                 quantity INT NOT NULL,
                 FOREIGN KEY (id_product)
                  REFERENCES product (id)
                   ON DELETE CASCADE
                    ON UPDATE CASCADE,
                    FOREIGN KEY (id_shop)
                    REFERENCES shop (id)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE);